# OnePlus One
**Codename :** bacon  
**Recommended Gapps** : [OpenGapps](https://opengapps.org/)

## Recoveries
You can use these recoveries to flash your OnePlus One

* [LineageOS 18.1 Recovery (Android 11/+)](https://download.lineageos.org/bacon) - Flash with `fastboot flash boot recovery.img`
* [Official TWRP (Android 9 only for 3.5 and later)](https://twrp.me/oneplus/oneplusone.html)

## Official ROMs
Official ROMs are directly visible on ROMs websites

* [LineageOS 18.1 (Android 11) (UPGRADE POSSIBLE)](https://wiki.lineageos.org/devices/bacon/) - Vanilla (doesn't support signature spoofing) + Gapps can be flashed
* [LineageOS for microG 18.1 (Android 11) (UPGRADE POSSIBLE)](https://download.lineage.microg.org/bacon/) - Vanilla + microG
* [HavocOS 3.12 (Android 10)](https://havoc-os.com/device#bacon) - Vanilla (supports signature spoofing)/Gapps
* [CarbonROM 7.0 (Android 9)](https://get.carbonrom.org/device-bacon.html) - Vanilla (supports signature spoofing) + Gapps can be flashed
* [ArrowOS 9.x (Android 9)](https://arrowos.net/download) - Vanilla (supports signature spoofing) + Gapps can be flashed

## Unofficial ROMs
Unofficial ROMs are made by the community. You can find them on the XDA Developers Forum.

* [HavocOS 4.5 (Android 11)](https://forum.xda-developers.com/t/rom-11-unoffical-havoc-os-4-5-for-the-oneplus-one-bacon.4285767/) - Vanilla (supports signature spoofing) + Gapps can be flashed
* [crDroid 5 (Android 9)](https://forum.xda-developers.com/t/rom-9-pie-bacon-unofficial-crdroid-5-5-6-16-2019.3940383/) - Vanulla (supports signature spoofing) + Gapps can be flashed




