# Welcome to ROMHub!

This is the most complete registry for finding custom ROMs for your Android Phone.

## Brands - use Ctrl+F to search

### Oneplus
* [OnePlus One (bacon)](roms/bacon.md)
* [OnePlus 2 (oneplus2)](roms/oneplus2.md)
* [OnePlus 3/3T (oneplus3)](roms/oneplus3.md)
* [OnePlus 5 (cheeseburger)](roms/cheeseburger.md)
* [OnePlus 5t (dumpling)](roms/dumpling.md)
* [OnePlus 6 (enchilada)](roms/enchilada.md)
* [OnePlus 6t (fajita)](roms/fajita.md)
* [OnePlus 7 (guacamoleb)](roms/guacamoleb.md)
* [OnePlus 7 Pro (guacamole)](roms/guacamole.md)
* [OnePlus 7t (hotdogb)](roms/hotdogb.md)
* [OnePlus 7t Pro (hotdog)](roms/hotdog.md)
* [OnePlus 8 (instantnoodle)](roms/instantnoodle.md)
* [OnePlus 8 Pro (instantnoodlep)](roms/instantnoodlep.md)
* [OnePlus 8t (kebab)](roms/kebab.md)
* [OnePlus 9 (lemonade)](roms/lemonade.md)
* [OnePlus 9 Pro (lemonadep)](roms/lemonadep.md)
* [OnePlus Nord (avicii)](roms/avicii.md)
* [OnePlus Nord N200 (dre)](roms/dre.md)
* [OnePlus X (onyx)](roms/onyx.md)